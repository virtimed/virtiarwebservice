﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Virti.Web.Models;
using Virti.Web.Api.Custom;

namespace Virti.Web.DataAccess
{
    public abstract class BaseContext
    {
        protected readonly IMongoDatabase _database = null;

        public BaseContext(IOptions<DBSettings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if (client != null)
                _database = client.GetDatabase(settings.Value.Database);
        }
    }

    public class DBContext<T> : BaseContext
    {
        public DBContext(IOptions<DBSettings> settings) : base(settings) { _collectionName = WebApiInfo.Tags[typeof(T)]; }

        public IMongoCollection<T> Collection
        {
            get
            {
                return _database.GetCollection<T>(_collectionName);
            }
        }

        private string _collectionName;
    }
}

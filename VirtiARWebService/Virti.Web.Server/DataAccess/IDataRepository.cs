﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;

namespace Virti.Web.DataAccess
{
    public interface IDataRepository<T>
    {
        Task<IEnumerable<T>> GetList();
        Task<T> GetItem(string id);
        Task<T> CreateItem(T item);
        Task<DeleteResult> DeleteItem(string id);
        Task<ReplaceOneResult> UpdateItem(string id, T body);

        // should be used with high cautious, only in relation with demo setup
        Task<DeleteResult> RemoveAll();
    }

    public interface IData
    {
        [BsonRepresentation(BsonType.ObjectId)]
        string _id { get; set; }
    }
}

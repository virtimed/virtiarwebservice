﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Virti.Web.Models;

namespace Virti.Web.DataAccess
{  
    public class DataAccessor<T>
    {
        private DBRepository<T> _repo;
        public DataAccessor(IOptions<DBSettings> settings)
        {
            _repo = new DBRepository<T>(settings);
        }

        public IEnumerable<T> GetList()
        {
            return _repo.GetList().Result;
        }

        public T GetItem(string id)
        {
            return _repo.GetItem(id).Result;
        }

        public Task<T> CreateItem(T value)
        {   
            return _repo.CreateItem(value);
        }

        public Task<ReplaceOneResult> UpdateItem(string id, T value)
        {          
            return _repo.UpdateItem(id, value);
        }
        public Task<DeleteResult> Delete(string id)
        {
            return _repo.DeleteItem(id);
        }
    }
}

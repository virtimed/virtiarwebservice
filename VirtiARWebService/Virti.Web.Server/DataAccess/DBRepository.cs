﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using MongoDB.Bson;
using Virti.Web.Models;

namespace Virti.Web.DataAccess
{
    public class DBRepository<T> : IDataRepository<T>
    {
        protected readonly DBContext<T> _context = null;

        public DBRepository(IOptions<DBSettings> settings)
        {
            _context = new DBContext<T>(settings);
        }

        public virtual async Task<IEnumerable<T>> GetList()
        {
            try
            {
                return await _context.Collection.Find(_ => true).ToListAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        //abstract public Task<T> GetItem(string id);
        //abstract public Task<DeleteResult> DeleteItem(string id);
        //abstract public Task<UpdateResult> UpdateItem(string id, T Item);

        public virtual async Task<T> CreateItem(T item)
        {
            try
            {
                await _context.Collection.InsertOneAsync(item);               
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
            return item;
        }

        public virtual async Task<T> GetItem(string id)
        {
            var filter = Builders<T>.Filter.Eq("Id", id);

            try
            {
                return await _context.Collection
                                .Find(filter)
                                .FirstOrDefaultAsync();
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public virtual async Task<DeleteResult> DeleteItem(string id)
        {
            try
            {
                return await _context.Collection.DeleteOneAsync(
                     Builders<T>.Filter.Eq("Id", id));
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public virtual async Task<ReplaceOneResult> UpdateItem(string id, T Item)
        {
            var filter = Builders<T>.Filter.Eq("Id", id);
            
            //var update = Builders<T>.Update.R(s => s, Item);

            try
            {
                return await _context.Collection.ReplaceOneAsync(filter, Item);
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }

        public async Task<DeleteResult> RemoveAll()
        {
            try
            {
                return await _context.Collection.DeleteManyAsync(new BsonDocument());
            }
            catch (Exception ex)
            {
                // log or manage the exception
                throw ex;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Virti.Web.Api.Custom
{
    public class CustomVector3
    {
        public float X;
        public float Y;
        public float Z;

        public CustomVector3(float _x, float _y, float _z) { X = _x; Y = _y; Z = _z; }
    }

    public class CustomQuaternion
    {
        public float X;
        public float Y;
        public float Z;
        public float W;

        public CustomQuaternion(float _x, float _y, float _z, float _w) { X = _x; Y = _y; Z = _z; W = _w; }
    }
}

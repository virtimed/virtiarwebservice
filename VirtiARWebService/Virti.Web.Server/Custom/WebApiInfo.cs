﻿using System;
using System.Collections.Generic;


namespace Virti.Web.Api.Custom
{
    public class WebApiInfo
    {
        public static string BaseUri = "http://localhost:30686/"; 

        //Class Type with Controller Name
        public static Dictionary<Type, string> Tags = new Dictionary<Type, string> {
            //Add your CUSTOM classes here to make it as API
             { typeof(SceneObjectApi), "SceneObject" },
             { typeof(SceneApi), "Scene" },            
             { typeof(AWSStorageAuthApi), "AWSStorageAuth" },
             //{ typeof(SimmanApi), "Simman" },
             //{ typeof(SimmanProcessApi), "SimmanProcess" },
             //{ typeof(SimmanEffectApi), "SimmanEffect" },
            //Add your CUSTOM classes here to make it as API
        };
    }
}

﻿using System;
using System.Collections.Generic;

namespace Virti.Web.Api.Custom
{
    //Scene Builder API
    public class SceneObjectApi : Identifier
    {
        public string AssetName { get; set; }
        public string BundleUri { get; set; }
        public string UIImagePath { get; set; }
        public CustomVector3 Position { get; set; }
    }

    public class SceneApi : Identifier
    {
        public string SceneName { get; set; }
        public string UIImagePath { get; set; }
        public List<SceneObjectApi> SceneObjectList { get; set; }
    }

    public class AWSStorageAuthApi : Identifier
    {
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string BucketName { get; set; }
    }

    //////VIRTI SIMMAN API  
    //public class SimmanApi : Identifier
    //{
    //    public string Title { get; set; }
    //    public string Description { get; set; }
    //    public string Created { get; set; }
    //}

    //public class SimmanProcessApi : Identifier
    //{
    //    public string Status { get; set; }
    //    public float Time { get; set; }

    //    public string Airway { get; set; }
    //    public string Speaking { get; set; }
    //    public string ChestAscult { get; set; }
    //    public string Eyes { get; set; }

    //    public float RespiratoryRate { get; set; }
    //    public float BPSystolic { get; set; }
    //    public float BPDiastolic { get; set; }
    //    public float HeartRate { get; set; }
    //    public float OxygenSatAir { get; set; }
    //    public float OxygenSatOxy { get; set; }
    //}

    //public class SimmanEffectApi : Identifier
    //{
    //    public string Title { get; set; }
    //    public string Description { get; set; }
    //    public float EffectValue { get; set; }
    //    public string ImageURL { get; set; }
    //}
}

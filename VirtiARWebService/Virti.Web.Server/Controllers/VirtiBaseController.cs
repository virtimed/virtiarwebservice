﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Virti.Web.DataAccess;
using Virti.Web.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Newtonsoft.Json;

namespace Virti.Web.Controllers
{
    public class VirtiBaseController<T> : Controller
    {
        private DataAccessor<T> _dataAcc;

        public VirtiBaseController(IOptions<DBSettings> settings)
        {
            _dataAcc = new DataAccessor<T>(settings);
        }

        [HttpGet]
        public IEnumerable<T> Get()
        {
            var dbData = _dataAcc.GetList();          
            return BusinessLogicList(dbData);
        }

        [HttpGet("{id}")]
        public T Get(string id)
        {
            var dbData = _dataAcc.GetItem(id);           
            return BusinessLogicItem(dbData);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]T value)
        {
            var resobj = await _dataAcc.CreateItem(value);
            return Ok(resobj);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] T value)
        {
            var retmsg = "Item Updated";
            try { await _dataAcc.UpdateItem(id, value); } catch (Exception e) { retmsg = e.Message; }            
            return Ok(retmsg);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var retmsg = "Item Deleted";
            try {
                //Respective files needs to be deleted TO BE Implemented
                DeleteFile(_dataAcc.GetItem(id));
                await _dataAcc.Delete(id);
            } catch (Exception e) { retmsg = e.Message; }            

            return Ok(retmsg);
        }

        [HttpPost, Route("UploadFile"), Consumes("multipart/form-data")]
        public async Task<IActionResult> UploadFile([FromForm] ICollection<IFormFile> files,[FromForm] string obj)
        {
            var value =  JsonConvert.DeserializeObject<T>(obj);

            foreach (var file in files)
            {
                if (file == null || file.Length == 0)
                    return Content("file not selected");

                var path = GetLocalDirectory(value, file.FileName);

                Directory.CreateDirectory(Path.GetDirectoryName(path));

                using (var stream = new FileStream(path + ".zip", FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                //UNZIPPING NEEDS TO BE DONE and files to be sent S3
            }

            //Create an entry after uploaded the files succesfully
            var resobj = await _dataAcc.CreateItem(value);
            return Ok(resobj);
        }

        public virtual string GetLocalDirectory(T item, string FileName)
        {
            return Path.Combine(Directory.GetCurrentDirectory(), "wwwroot",
                           FileName);           
        }

        public virtual bool DeleteFile(T item)
        {
            return true;
        }

        public virtual IEnumerable<T> BusinessLogicList(IEnumerable<T> dbData)
        {
            return dbData;
        }

        public virtual T BusinessLogicItem(T dbData)
        {
            return dbData;
        }
    }
}
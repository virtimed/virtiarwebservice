﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Virti.Web.Models
{
    public class DBSettings
    {
        public string ConnectionString;
        public string Database;
    }
}

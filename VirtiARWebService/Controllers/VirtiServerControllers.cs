﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using Virti.Web.Models;
using Virti.Web.Controllers;
using Virti.Web.Api.Custom;

namespace VirtiARWebService.Controllers
{
    //NO BUSINESS LOGIC CONTROLLERS CAN BE DECLARED LIKE THIS
    [Route("api/[controller]")]
    public class SceneObjectController : VirtiBaseController<SceneObjectApi>
    {
        public SceneObjectController(IOptions<DBSettings> settings) : base(settings) { }
    }

    [Route("api/[controller]")]
    public class SceneController : VirtiBaseController<SceneApi>
    {
        public SceneController(IOptions<DBSettings> settings) : base(settings) { }
    }

    [Route("api/[controller]")]
    public class AWSStorageAuthController : VirtiBaseController<AWSStorageAuthApi>
    {
        public AWSStorageAuthController(IOptions<DBSettings> settings) : base(settings) { }
    }

    //[Route("api/[controller]")]
    //public class SimmanController : VirtiBaseController<SimmanApi>
    //{
    //    public SimmanController(IOptions<DBSettings> settings) : base(settings) { }
    //}

    //[Route("api/[controller]")]
    //public class SimmanProcessController : VirtiBaseController<SimmanProcessApi>
    //{
    //    public SimmanProcessController(IOptions<DBSettings> settings) : base(settings) { }
    //}

    //[Route("api/[controller]")]
    //public class SimmanEffectController : VirtiBaseController<SimmanEffectApi>
    //{
    //    public SimmanEffectController(IOptions<DBSettings> settings) : base(settings) { }
    //}
}
